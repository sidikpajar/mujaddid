<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <?php 
        $userid = $_SESSION['userid'];
        $personalSidebar="SELECT * FROM users WHERE userid='$userid' ";
        $querySidebar = mysqli_query( $connect, $personalSidebar );
        while($row = mysqli_fetch_array( $querySidebar )) {
      ?>
      <div class="pull-left image">
        <img src="../files-photo/<?php echo $row['url_foto'];?>" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php echo $row['name'];?> </p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
        <?php } ?>
    </div>

    <ul class="sidebar-menu" data-widget="tree">
      <li class="header" style="background-color:#c6e6ee">Menu Utama</li>
      <li><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
      <li><a href="acara.php"><i class="fa fa-dashboard"></i> <span>Acara</span></a></li>
      <li><a href="personal-me.php"><i class="fa fa-user"></i> <span>Data Pribadi</span></a></li>
      <li class="header" ></li>
      <li class="header" style="background-color:#c6e6ee">Mahasiswa Baru</li>
      <li><a href="pendaftaran.php"><i class="fa fa-user"></i> <span>Pendaftaran Rohis</span></a></li>

      <li class="header" ></li>
      <li class="header" style="background-color:#c6e6ee">Pengaturan Organisasi</li>

      <li class="treeview">
        <a href="#">
          <i class="fa fa-edit"></i>
          <span>Data Master</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="data-master-anggota.php"><i class="fa fa-desktop"></i>Anggota Rohis</a></li>
          <li><a href="data-master-jurusan.php"><i class="fa fa-desktop"></i>Pengaturan Jurusan</a></li>
          <li><a href="data-master-periode.php"><i class="fa fa-desktop"></i>Pengaturan Periode Organisasi</a></li>
          <li><a href="data-master-struktur.php"><i class="fa fa-desktop"></i>Struktur Organisasi</a></li>
          <li><a href="data-master-dokumen.php"><i class="fa fa-desktop"></i>Dokumen</a></li>
        </ul>
      </li>

      <li class="header"></li>
      <li><a href="logout.php"><i class="fa fa-sign-out text-red"></i> <span>Logout</span></a></li>
      <li><a href="change-password.php"><i class="fa fa-key text-yellow"></i> <span>Change Password</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
