<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Daftar Paket Wisata
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Daftar Paket Wisata</li>
      </ol>
    </section>
    <section class="content-header">
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
              <!-- <div style="padding-top:20px">
                <a class="btn btn-primary" href="add-user.php" >Add User</a>
              </div> -->
            </div>
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-12 table-responsive">
                      <iframe id="txtArea1" style="display:none"></iframe>
                      <table style="font-size:12px;  cellpadding:3"  id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                          <th>ID Agen</th>
                          <th>Nama Agen</th>
                          <th>Paket Wisata</th>
                          <th>Date Submit</th>
                          <th>Status Paket</th>
                          <th>Status SlideShow</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                          <?php
                            $sql="SELECT * FROM tbl_place
                            INNER JOIN personal
                            ON personal.personal_id = tbl_place.id_agent
                            ";
                            $query = mysqli_query($connect,$sql);
                            while($row = mysqli_fetch_array($query)) {
                          ?>
                          
                          <tr role="row" class="odd">
                            <td ><?php echo $row['id_agent'] ?></td>
                            <td ><?php echo $row['personal_nama'] ?></td>
                            <td ><?php echo $row['nama_place'] ?></td>
                            <td ><?php echo $row['tanggal'] ?></td>
                            <td ><?php 
                           
                                  if( $row['status'] == 'Y'){
                                    echo "Active";
                                  } else {
                                    echo "non-Active";
                                  }
                            
                            ?></td>
                            <td ><?php 
                                  if( $row['slider'] == '1'){
                                    echo "<a href='slideshow-update-to-off.php?id_place=".$row['id_place']." ' class='btn btn-success btn-xs'>Active<a> ";
                                  } else {
                                    echo "<a href='slideshow-update-to-on.php?id_place=".$row['id_place']." ' class='btn btn-danger btn-xs'>Non-Active<a> ";
                                  }
                            ?></td>
                            <td>
                              <?php
                                  echo "<a href='detail.php?id_place=".$row['id_place']."' class='btn btn-xs btn-success'>Detail</a>";
                              ?>
                            </td>
                          </tr>
                          <?php
                            }
                           ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="box-footer">
              </div>
            </div>
          </div>
    </section>
  </div>
  </div>
  <script>
      function printData()
        {
          var divToPrint=document.getElementById("example1");
          newWin= window.open("");
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
        }
  </script>
  <?php
  include("component/footer.php");
   ?>
