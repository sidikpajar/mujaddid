<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
<div class="content-wrapper">
	<section class="content-header">
	<h1>Data Pribadi </h1> 
	<ol class="breadcrumb">
		<li>
			<a href="index.php"><i class="fa fa-dashboard"></i> Data Pribadi</a>
		</li>
		<li class="active">Data Pribadi</li>
	</ol>
	</section>
	<section class="content">
    <div class="row">
      <?php
        $userid = $_SESSION['userid'];
      
        $personalMe="SELECT * FROM users WHERE userid='$userid' ";
        $queryPersonalMe = mysqli_query( $connect, $personalMe );
        while($row = mysqli_fetch_array( $queryPersonalMe )) {
      ?>
        <div class="col-md-4">
          
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">
                  NIM/Kode Identitas: <?php echo $row['userid']; ?>
              </h3>
              
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12" style="text-align:center">
                  <img style="width:200px" src="../files-photo/<?php echo $row['url_foto'];?>">
                  <br/>
                  <a href="personal-me-foto-upload.php">Change Picture</a>
                </div>
              </div>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-pills nav-stacked">
                <li><a href='#'>Username <span class='pull-right'> <?php echo $row['user_name'];?></span></a></li>
                <li><a href='#'>Hak Akses <span class='pull-right'> <?php echo $row['user_level'];?></span></a></li>
                <li><a href='#'>Nama Lengkap <span class='pull-right'> <?php echo $row['name'];?></span></a></li>
                <li><a href='#'>Jenis Kelamin <span class='pull-right'> <?php echo $row['gender'];?></span></a></li>
                <li><a href='#'>Birthday <span class='pull-right'> <?php echo $row['birthday'];?></span></a></li>
                <li><a href='#'>Kata Sandi <span class='pull-right'> <?php echo $row['user_password'];?></span></a></li>
              </ul>
            </div>
          </div>
        </div>
      <?php } ?>
      
        <div class="col-md-4">
          <div class="box box-warning">
            <div class="box-header with-border">
              <h3 class="box-title">Pengaturan Alamat</h3>
              <a href="personal-me-alamat-add.php" class="pull-right btn btn-xs btn-primary">Tambah Alamat</a>
            </div>
            <div class="box-body">
              <ul class="nav nav-pills nav-stacked">
                <?php
                  $personalMe="SELECT * FROM users_alamat WHERE userid='$userid' ";
                  $queryPersonalMe = mysqli_query( $connect, $personalMe );
                  while($row = mysqli_fetch_array( $queryPersonalMe )) {
                ?>
                <p>
                  <b>
                    <?php 
                      echo $row['title'];
                      if($row['status']==1){
                        echo "";
                      } else {
                        echo "<small><i style='color:red'> - Dinonaktifkan </i></small>";
                      }
                    ?> 
                  </b>
                    <a style="margin:5px" href="personal-me-alamat-delete.php?id=<?php echo $row['id']; ?>" class="pull-right"><i style="color:red" class="fa fa-times"></i></a>
                    <a style="margin:5px" href="personal-me-alamat-edit.php?id=<?php echo $row['id']; ?>" class="pull-right"><i style="color:black" class="fa fa-edit"></i></a>
                    <br/>
                    <span class=''> <?php echo $row['value'];?></span>
                  </p>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>

        <div class="col-md-4">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Pengaturan Kontak</h3>
              <a href="personal-me-kontak-add.php" class="pull-right btn btn-xs btn-primary">Tambah Kontak</a>
            </div>
            <div class="box-body">
              <ul class="nav nav-pills nav-stacked">
                <?php
                  $personalMe="SELECT * FROM users_kontak WHERE userid='$userid' ";
                  $queryPersonalMe = mysqli_query( $connect, $personalMe );
                  while($row = mysqli_fetch_array( $queryPersonalMe )) {
                ?>
                <p>
                  <b>
                    <?php 
                      echo $row['title'];
                      if($row['status']==1){
                        echo "";
                      } else {
                        echo "<small><i style='color:red'> - Dinonaktifkan </i></small>";
                      }
                    ?> 
                  </b>
                    <a style="margin:5px" href="personal-me-kontak-delete.php?id=<?php echo $row['id']; ?>" class="pull-right"><i style="color:red" class="fa fa-times"></i></a>
                    <a style="margin:5px" href="personal-me-kontak-edit.php?id=<?php echo $row['id']; ?>" class="pull-right"><i style="color:black" class="fa fa-edit"></i></a>
                    <br/>
                    <span class=''> <?php echo $row['value'];?></span>
                </p>
                <?php } ?>
              </ul>
            </div>
          </div>
        </div>

    </div>
	</section>
</div>
</div>
<script>
      function printData()
        {
          var divToPrint=document.getElementById("example1");
          newWin= window.open("");
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
        }
  </script>
<?php
  include("component/footer.php");
   ?>