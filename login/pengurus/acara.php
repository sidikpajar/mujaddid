<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Daftar Acara
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Acara</li>
      </ol>
    </section>
    <section class="content-header">
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
             
              <div style="padding-top:20px">
                <a class="btn btn-primary" href="add-acara.php" >Tambah Acara</a>
              </div>
            </div>
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-12 table-responsive">
                      <iframe id="txtArea1" style="display:none"></iframe>
                      <table style=""  id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                          <th>Acara ID</th>
                          <th>Nama Acara</th>
                          <th>Poster</th>
                          <th>Jumlah Pendaftar</th>
                          <th>Tanggal Mulai</th>
                          <th>Tanggal Selesai</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                            $sql="SELECT * FROM tbl_acara";
                            $query = mysqli_query($connect,$sql);
                            while($row = mysqli_fetch_array($query)) {
                            ?>
                          <tr role="row" class="odd">
                            <td ><?php echo $row['id_acara'] ?></td>
                            <td ><?php 
                              if ($row['poster']==null){
                                echo "<a class='btn btn-primary btn-xs' href='add.poster.php?id_acara=".$row['id_acara']." '> Tambah Poster </a>";
                              } else {
                                echo '<img width="100px" height="135px" src="data:image/jpeg;base64,'.base64_encode( $row['poster'] ).'"/>';
                                echo "<br/>";
                                echo "<a style='margin-top :5px' class='btn btn-warning btn-block btn-xs' href='add.poster.php?id_acara=".$row['id_acara']." '> Ubah Poster </a>";
                              }
                            
                            ?></td>
                            <td ><?php echo $row['nama_acara'] ?></td>
                            <td ></td>
                            <td ><?php echo $row['tanggal_mulai'] ?></td>
                            <td ><?php echo $row['tanggal_selesai'] ?></td>
                            <td >
                            <?php 
                              // cek status, jika status 1 maka akan muncul Aktif, jika 0 maka muncul Tidak aktif
                              if ($row['status']==1){
                                echo "<a class='btn btn-success btn-xs' '> Aktif </a>";
                              } else echo "<a class='btn btn-danger btn-xs' '> Tidak Aktif </a>";
                              ?>
                            </td>
                            <td>
                              <?php
                                  echo "<a href='detail-acara.php?id_acara=".$row['id_acara']."' class='btn btn-xs btn-success'>Detail</a>";
                              ?>
                              <br/>
                              <?php
                                  echo "<a href='edit.php?id_acara=".$row['id_acara']."' class='btn btn-xs btn-warning'>Ubah Data</a>";
                              ?>
                              <br/>
                              <?php
                                  echo "<a href='delete.php?id_acara=".$row['id_acara']."' class='btn btn-xs btn-danger'>Hapus Data</a>";
                              ?>
                            </td>
                          </tr>
                          <?php
                            }
                           ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="box-footer">
              </div>
            </div>
          </div>
    </section>
  </div>
  </div>
  <script>
      function printData()
        {
          var divToPrint=document.getElementById("example1");
          newWin= window.open("");
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
        }
  </script>
  <?php
  include("component/footer.php");
   ?>
