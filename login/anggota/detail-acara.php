<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Detail Acara The Mujaddid
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Detail Acara</li>
      </ol>
    </section>
    <section class="content-header">
    </section>
    <section class="content">
      <div class="row">
        <div class="container">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#acara" data-toggle="tab" aria-expanded="true">Acara</a></li>
              <li class=""><a href="#deskripsi" data-toggle="tab" aria-expanded="true">Deskripsi Acara</a></li>
              <li class=""><a href="#pendaftar" data-toggle="tab" aria-expanded="false">Pendaftar</a></li>
            </ul>
            <div class="tab-content">

              <div class="tab-pane active" id="acara">

              <table class="table table-bordered">
                <tbody>
                  <tr>
                    <th>Keterangan</th>
                    <th>Isi</th>
                  </tr>
                    <?php
                      $id_acara = $_GET['id_acara'];
                      $sql1="SELECT * FROM tbl_acara WHERE id_acara='$id_acara' ";
                      $query = mysqli_query( $connect, $sql1 );
                      while($row = mysqli_fetch_array( $query )) {
                    ?>
                  <tr>
                    <td>ID Acara</td>
                    <td><?php echo $row['id_acara']; ?></td>
                  </tr>
                  <tr>
                    <td>Nama Acara</td>
                    <td><?php echo $row['nama_acara']; ?></td>
                  </tr>
                  <tr>
                    <td>Tanggal Mulai</td>
                    <td><?php echo $row['tanggal_mulai']; ?></td>
                  </tr>
                  <tr>
                    <td>Tanggal Selesai</td>
                    <td><?php echo $row['tanggal_selesai']; ?></td>
                  </tr>
                  <tr>
                    <td>Tempat</td>
                    <td><?php echo $row['tempat']; ?></td>
                  </tr>
                  <tr>
                    <td>Status</td>
                    <td >
                      <?php 
                        // cek status, jika status 1 maka akan muncul Aktif, jika 0 maka muncul Tidak aktif
                        if ($row['status']==1){
                          echo "<a class='btn btn-success btn-xs' '> Aktif </a>";
                        } else echo "<a class='btn btn-danger btn-xs' '> Tidak Aktif </a>";
                      ?>
                    </td>
                  </tr>

                  <?php } ?>

                </tbody>
             </table>


            

              </div>

              <div class="tab-pane" id="deskripsi">
              <table class="table table-bordered">
                <tbody>
                  <tr>
                    <th>deskripsi</th>
                  </tr>
                    <?php
                      $id_acara = $_GET['id_acara'];
                      $sql2="SELECT * FROM tbl_acara
        
                      WHERE id_acara='$id_acara' ";
                      $query2 = mysqli_query( $connect, $sql2 );
                      while($row = mysqli_fetch_array( $query2 )) {
                    ?>
                  <tr>
                    <td><?php echo $row['deskripsi']; ?></td>
                  </tr>

                  <?php } ?>

                </tbody>
              </table>

              </div>

         
              <div class="tab-pane" id="pendaftar">
              <table class="table table-bordered">
                <tbody>
                  <tr>
                    <th>Kode Pendaftaran</th>
                    <th>Nama Acara</th>
                    <th>Nama Pendaftar</th>
                    <th>Email</th>
                    <th>Institute</th>
                    <th>Tanggal Daftar</th>
                  </tr>
                    <?php
                      $id_acara = $_GET['id_acara'];
                      $sql3="SELECT 
                        tbl_pendaftaran_acara.kode as KodePendaftaran,
                        tbl_acara.nama_acara as NamaAcara,
                        tbl_pendaftaran_acara.nama as NamaPendaftar,
                        tbl_pendaftaran_acara.email as Email,
                        tbl_pendaftaran_acara.institute as Institute,
                        tbl_pendaftaran_acara.tanggal_daftar as TanggalDaftar
                      FROM tbl_pendaftaran_acara
                      INNER JOIN tbl_acara
                        ON tbl_acara.id_acara = tbl_pendaftaran_acara.id_acara
                  
                      WHERE tbl_acara.id_acara='$id_acara' ";
                      $query3 = mysqli_query( $connect, $sql3 );
                      while($row = mysqli_fetch_array( $query3 )) {
                    ?>
                  <tr>
                    <td><?php echo $row['KodePendaftaran']; ?></td>
                    <td><?php echo $row['NamaAcara']; ?></td>
                    <td><?php echo $row['NamaPendaftar']; ?></td>
                    <td><?php echo $row['Email']; ?></td>
                    <td><?php echo $row['Institute']; ?></td>
                    <td><?php echo $row['TanggalDaftar']; ?></td>
                  </tr>

                  <?php } ?>

                </tbody>
              </table>
              </div>
             

              <div class="tab-pane" id="foto">
                 
                  <?php
                    $id_place = $_GET['id_place'];
                    $sql2="SELECT * FROM tbl_foto
                    WHERE id_place='$id_place' ";
                    $query2 = mysqli_query( $connect, $sql2 );
                    while($row = mysqli_fetch_array( $query2 )) {
                        echo "
                          <span>
                          <img src='../tenant/files/".$row['url_foto']."' style='width:250px; height:auto; border-radius:4px;'> 
                          </span>
                          ";
                    }
                  ?>  
                
              </div>
            </div>
          
          </div>           
        </div>
      </div>
    </section>
  </div>
  </div>
  <script>
      function printData()
        {
          var divToPrint=document.getElementById("example1");
          newWin= window.open("");
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
        }
  </script>
  <?php
  include("component/footer.php");
   ?>
