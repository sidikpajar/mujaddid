<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-12 shadow-lg" style="margin-top:100px;">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Tambah Kegiatan Acara</h3>
                  </div>
                  <form role="form" method="POST" action="add-acara.php" enctype="multipart/form-data">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Acara</label>
                        <input type="text" class="form-control" id="nama_acara" name="nama_acara" required>
                      </div>
                      
                       <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Mulai</label>
                        <input type="date" class="form-control" id="tanggal_mulai" name="tanggal_mulai" required>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Selesai</label>
                        <input type="date" class="form-control" id="tanggal_selesai" name="tanggal_selesai" required>
                      </div>

                       <div class="form-group">
                        <label for="exampleInputEmail1">Tempat</label>
                        <input type="text" class="form-control" id="tempat" name="tempat" required>
                      </div>


                       <div class="form-group">
                        <label for="exampleInputEmail1">Deskripsi Acara</label>
                        <div class="box-body pad">
                          <textarea id="editor1" name="deskripsi" rows="10" cols="80" style="visibility: hidden; display:none;"> 
                          </textarea>
                       </div>
                      </div>

                    </div>
                    <div class="box-footer">
                      <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                    </div>
                    <?php
                    if(isset($_POST["submit"])) {
                      $id_acara                 = rand(00000,999999999);
                      $status                   = 1;
                      $md5_id_acara             = md5($id_acara);
                      $nama_acara               = $_POST['nama_acara'];
                      $tanggal_mulai            = $_POST['tanggal_mulai'];
                      $tanggal_selesai          = $_POST['tanggal_selesai'];
                      $tempat                   = $_POST['tempat'];
                      $deskripsi                = $_POST['deskripsi'];
                      $sql = "INSERT INTO tbl_acara (id_acara, nama_acara, tanggal_mulai, tanggal_selesai, tempat, deskripsi, status)
                              VALUES ('$md5_id_acara','$nama_acara','$tanggal_mulai', '$tanggal_selesai', '$tempat', '$deskripsi', '$status')
                              ";
                      if ($connect-> query($sql) === TRUE ) {
                        echo "
                        <script type='text/javascript'>
                            alert('Acara ".$nama_acara." Berhasil ditambah');
                            window.location = 'acara.php';
                        </script>";
                        } else {
                        echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                        }
                        $connect->close();
                        }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
