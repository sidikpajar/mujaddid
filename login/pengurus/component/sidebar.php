<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p><?php
          $user_name = $_SESSION['user_name'];
          echo $user_name;
        ?></p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
      </div>
    </div>

    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">SIDE BAR NAVIGATION</li>
      <li><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
      <li><a href="acara.php"><i class="fa fa-dashboard"></i> <span>Acara</span></a></li>
      <li><a href="pendaftaran-peserta.php"><i class="fa fa-user"></i> <span>Pendaftaran Peserta Acara</span></a></li>
      <li><a href="dokumen.php"><i class="fa fa-user"></i> <span>Dokumen</span></a></li>
      <li><a href="edit-profil.php"><i class="fa fa-user"></i> <span>Edit Profil</span></a></li>
      <li class="header"></li>
      <li><a href="logout.php"><i class="fa fa-sign-out text-red"></i> <span>Logout</span></a></li>
      <li><a href="change-password.php"><i class="fa fa-key text-yellow"></i> <span>Change Password</span></a></li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
