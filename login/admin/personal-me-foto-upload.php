<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-12 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <h3 class="box-title">Upload Foto</h3>
                  </div>
                  <form role="form" method="POST" action="personal-me-foto-upload.php" enctype="multipart/form-data">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1"> Upload Foto : </label> <i><b style="color:red">*</b>jpg/png</i>
                        <input type="file" name="url_foto" required>
                      </div>

                      
                    </div>
                    <div class="box-footer">
                      <a href="personal-me.php" class="btn btn-primary">Kembali</a> 
                      <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                    </div>

                      <?php
                        if(isset($_POST["submit"])){
                          $check = $_FILES["url_foto"]["tmp_name"];
                          if($check !== false){
                              $id_foto         = rand(100,99999);
                              $lokasi_file      = $_FILES['url_foto']['tmp_name'];
                              $nama_file        = $_FILES['url_foto']['name'];
                              $folder          = "../files-photo/$id_foto+$nama_file";
                              $userid          = $_SESSION['userid'];
                              $create_by       = $_SESSION['user_name'];
                              $create_at       = (new DateTime('now'))->format('Y-m-d H:i:s');

                              if (move_uploaded_file($lokasi_file,"$folder")){
                                echo "Nama File : <b>$nama_file</b> sukses di upload";
                                
                                $sql2 = "UPDATE users SET
                                        url_foto='$id_foto+$nama_file',
                                        create_by='$create_by',
                                        create_at='$create_at'
                                        WHERE userid = '$userid' ";

                                if ($connect-> query($sql2) === TRUE) {
                                    echo "
                                    <script type= 'text/javascript'>
                                        window.location = 'personal-me.php';
                                    </script>";
                                    } else {
                                        echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                                      }
                                }else{
                                    echo "<script type= 'text/javascript'>alert('File upload failed, please try again');</script>";
                                }
                              }
                              else{
                                echo "File gagal di upload";
                              }
                          }else{
                              echo "Please select an jpg/png file to upload.";
                          }
                      ?>

                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
