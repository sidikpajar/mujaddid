<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Anggota The Mujaddid
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Data Master</a></li>
        <li class="active">Anggota</li>
      </ol>
    </section>
    <section class="content-header">
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
            </div>
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-12 table-responsive">
                      <iframe id="txtArea1" style="display:none"></iframe>
                      <table style=""  id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                          <th>NIM</th>
                          <th>Nama Lengkap</th>
                          <th>Username</th>
                          <th>Password</th>
                          <th>Hak Akses</th>
                          <th>Jenis Kelamin</th>
                          <th>Ulang Tahun</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                            $sql="SELECT * FROM users";
                            $query = mysqli_query($connect,$sql);
                            while($row = mysqli_fetch_array($query)) {
                            ?>
                          <tr role="row" class="odd">
                            <td ><?php echo $row['userid'] ?></td>
                            <td ><?php echo $row['name'] ?></td>
                            <td ><?php echo $row['user_name'] ?></td>
                            <td ><?php echo $row['user_password'] ?></td>
                            <td ><?php echo $row['user_level'] ?></td>
                            <td ><?php echo $row['gender'] ?></td>
                            <td ><?php echo $row['birthday'] ?></td>
                            <td >
                              <?php 
                              // cek status, jika status 1 maka akan muncul Aktif, jika 0 maka muncul Tidak aktif
                              if ($row['status']==1){
                                echo "<a href='data-master-anggota-update-to-off.php?userid=".$row['userid']." ' class='btn btn-success  btn-xs' '> Aktif </a>";
                              } else echo "<a href='data-master-anggota-update-to-on.php?userid=".$row['userid']." ' class='btn btn-danger  btn-xs' '> Tidak Aktif </a>";
                              ?>
                            </td>
                            <td>
                              <?php
                                  echo "<a href='data-master-anggota-detail.php?userid=".$row['userid']."' class='btn btn-xs btn-primary'>Detail</a>";
                              ?>
                            </td>
                          </tr>
                          <?php
                            }
                           ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  </div>
  <script>
      function printData()
        {
          var divToPrint=document.getElementById("example1");
          newWin= window.open("");
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
        }
  </script>
  <?php
  include("component/footer.php");
   ?>
