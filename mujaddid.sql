-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 10, 2019 at 01:17 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mujaddid`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_acara`
--

CREATE TABLE `tbl_acara` (
  `id_acara` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `nama_acara` varchar(100) NOT NULL,
  `tanggal_mulai` datetime NOT NULL,
  `tanggal_selesai` datetime NOT NULL,
  `pembayaran` int(1) NOT NULL COMMENT '1 untuk berbayar, 0 untuk gratis',
  `biaya` bigint(8) DEFAULT NULL,
  `tempat` varchar(32) NOT NULL,
  `poster` text,
  `deskripsi` text NOT NULL,
  `status` tinyint(1) NOT NULL COMMENT '1 for active, 2 for non-active',
  `create_by` varchar(32) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_acara`
--

INSERT INTO `tbl_acara` (`id_acara`, `id_periode`, `nama_acara`, `tanggal_mulai`, `tanggal_selesai`, `pembayaran`, `biaya`, `tempat`, `poster`, `deskripsi`, `status`, `create_by`, `create_at`) VALUES
(3227, 1, 'Buka Puasa Bersama', '2010-10-10 10:00:00', '2010-10-10 20:00:00', 1, 100000, 'Kalbis Institute ke 2', '3415+ucapan-myindo.jpg', '<p>Yuk Ikuti Bukber ke 2</p>\r\n', 1, 'admin', '2019-06-05 00:53:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_acara_dokumentasi`
--

CREATE TABLE `tbl_acara_dokumentasi` (
  `id` int(11) NOT NULL,
  `id_acara` int(11) NOT NULL,
  `url_foto` text NOT NULL,
  `judul` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(32) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_acara_dokumentasi`
--

INSERT INTO `tbl_acara_dokumentasi` (`id`, `id_acara`, `url_foto`, `judul`, `status`, `create_by`, `create_at`) VALUES
(27669, 3227, '27669+Screen Shot 2019-05-23 at 09.46.26.png', 'Contoh Foto ke 3', 0, 'admin', '2019-06-05 00:55:27'),
(36340, 3227, '36340+ucapan-myindo.jpg', 'Kartu Ucapan', 0, 'admin', '2019-06-05 00:53:47'),
(89740, 3227, '89740+Screen Shot 2019-05-23 at 09.57.11.png', 'contoh foto ke2', 0, 'admin', '2019-06-05 00:54:47');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_acara_kontak`
--

CREATE TABLE `tbl_acara_kontak` (
  `id` int(11) NOT NULL,
  `id_acara` int(11) NOT NULL,
  `userid` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(32) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_acara_kontak`
--

INSERT INTO `tbl_acara_kontak` (`id`, `id_acara`, `userid`, `status`, `create_by`, `create_at`) VALUES
(1001, 3227, '2013100566', 1, 'System', '2019-06-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_dokumen`
--

CREATE TABLE `tbl_dokumen` (
  `id` int(11) NOT NULL,
  `id_acara` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `value` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(32) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jurusan`
--

CREATE TABLE `tbl_jurusan` (
  `kode` int(11) NOT NULL,
  `nama_jurusan` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(32) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jurusan`
--

INSERT INTO `tbl_jurusan` (`kode`, `nama_jurusan`, `status`, `create_by`, `create_at`) VALUES
(1002, 'Akuntansi (D3)', 1, 'fajar', '2019-06-06 00:00:00'),
(1172, 'Akuntansi (S1)', 1, 'admin', '2019-06-06 18:57:32'),
(2070, 'Game Computing & Technology (S1)', 1, 'admin', '2019-06-06 18:58:47'),
(2129, 'Advertising & Digital Communication (S1)', 1, 'admin', '2019-06-06 18:58:24'),
(2131, ' Desain Komunikasi Visual (S1)', 1, 'admin', '2019-06-06 18:58:31'),
(3431, 'Sistem Informasi (S1)', 1, 'admin', '2019-06-06 18:58:53'),
(3538, 'Broadcasting (S1)', 1, 'admin', '2019-06-06 18:58:11'),
(3812, 'Teknik Informatika (S1)', 1, 'admin', '2019-06-06 18:58:37'),
(3981, 'Arsitek (S1)', 1, 'admin', '2019-06-06 18:58:59'),
(4179, 'Business in Creative Industries (S1)', 1, 'admin', '2019-06-06 18:57:56'),
(6096, 'Strategic Communication (S1)', 1, 'admin', '2019-06-06 18:58:17'),
(8687, 'Matematika Bisnis', 1, 'admin', '2019-06-06 18:58:04'),
(9497, ' Manajemen (S1)', 1, 'admin', '2019-06-06 18:57:44');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pendaftaran_acara`
--

CREATE TABLE `tbl_pendaftaran_acara` (
  `kode` int(11) NOT NULL,
  `id_acara` varchar(32) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `institute` text NOT NULL,
  `tanggal_daftar` datetime NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pendaftaran_acara`
--

INSERT INTO `tbl_pendaftaran_acara` (`kode`, `id_acara`, `nama`, `email`, `institute`, `tanggal_daftar`, `status`) VALUES
(1021, '3227', 'M Fajar Sidik', '1995fajarsidik@gmail.com', 'Universitas Budi Luhur', '2019-05-31 00:00:00', 1),
(1022, '3227', 'Ryan', 'ryan@gmail.com', 'Universitas Budiluhur', '2019-06-01 00:00:00', 1),
(1023, '3227', 'Siska', 'siska@gmail.com', 'Sragen', '2019-06-01 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pendaftaran_anggota`
--

CREATE TABLE `tbl_pendaftaran_anggota` (
  `kode` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `jurusan` int(32) NOT NULL,
  `alasan` text NOT NULL,
  `email` varchar(100) NOT NULL,
  `tanggal_daftar` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pendaftaran_anggota`
--

INSERT INTO `tbl_pendaftaran_anggota` (`kode`, `nim`, `nama`, `jurusan`, `alasan`, `email`, `tanggal_daftar`) VALUES
(9912, 2011100577, 'Khairul Cahya', 1, 'Ingin mengikuti kegiatan kerohanian secara intensive', 'khairul@gmail.com', '2019-06-06 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pendaftaran_bayar`
--

CREATE TABLE `tbl_pendaftaran_bayar` (
  `id_pembayaran` int(11) NOT NULL,
  `id_acara` varchar(32) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` text NOT NULL,
  `foto` text NOT NULL,
  `pembayaran_via` varchar(100) NOT NULL,
  `jumlah` int(100) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_periode`
--

CREATE TABLE `tbl_periode` (
  `id` int(32) NOT NULL,
  `periode` varchar(32) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(32) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_periode`
--

INSERT INTO `tbl_periode` (`id`, `periode`, `status`, `create_by`, `create_at`) VALUES
(1, '2016/2017', 1, 'System', '2019-06-03 00:00:00'),
(2, '2017/2018', 1, 'System', '2019-06-03 00:00:00'),
(3, '2018/2019', 1, 'System', '2019-06-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_struktur`
--

CREATE TABLE `tbl_struktur` (
  `id` int(11) NOT NULL,
  `id_periode` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `jabatan` varchar(100) NOT NULL,
  `id_jurusan` int(11) DEFAULT NULL,
  `lv1` int(11) DEFAULT NULL,
  `lv2` int(11) DEFAULT NULL,
  `lv3` int(11) DEFAULT NULL,
  `lv4` int(11) DEFAULT NULL,
  `lv5` int(11) DEFAULT NULL,
  `lv6` int(11) DEFAULT NULL,
  `lv7` int(11) DEFAULT NULL,
  `lv8` int(11) DEFAULT NULL,
  `lv9` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(32) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_struktur`
--

INSERT INTO `tbl_struktur` (`id`, `id_periode`, `nim`, `jabatan`, `id_jurusan`, `lv1`, `lv2`, `lv3`, `lv4`, `lv5`, `lv6`, `lv7`, `lv8`, `lv9`, `status`, `create_by`, `create_at`) VALUES
(1, 1, 999999999, 'Pembina', 9497, 999999999, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'admin', '2019-06-06 00:00:00'),
(2, 1, 2015101955, 'Ketua Rohis', 9497, 999999999, 2015101955, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'admin', '2019-06-06 00:00:00'),
(3, 1, 2014101196, 'Sekretaris', 4179, 999999999, 2015101955, 2014101196, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'admin', '2019-06-06 00:00:00'),
(4, 1, 2015101753, 'Bendahara', 1172, 999999999, 2015101955, 2015101753, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'admin', '2019-06-06 00:00:00'),
(5, 1, 2015101794, 'Divisi Dana Usaha', 3431, 999999999, 2015101955, 2015101753, 2015101794, NULL, NULL, NULL, NULL, NULL, 1, 'admin', '2019-06-06 00:00:00'),
(6, 1, 2015101642, 'Divisi Dakwah', 9497, 999999999, 2015101955, 2015101642, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'admin', '2019-06-06 00:00:00'),
(7, 1, 2015102044, 'Divisi Seni', 3431, 999999999, 2015101955, 2015102044, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'admin', '2019-06-06 00:00:00'),
(8, 1, 2015101820, 'Divisi Kaderisasi', 1172, 999999999, 2015101955, 2015101820, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'admin', '2019-06-06 00:00:00'),
(9, 1, 2015101518, 'Divisi Humas', 9497, 999999999, 2015101955, 2015101518, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'admin', '2019-06-06 00:00:00'),
(10, 1, 2015101894, 'Divisi Design, Publikasi, dan Dokumentasi', 3431, 999999999, 2015101955, 2015101894, NULL, NULL, NULL, NULL, NULL, NULL, 1, 'admin', '2019-06-06 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userid` int(11) NOT NULL,
  `user_name` varchar(32) NOT NULL,
  `user_level` varchar(32) NOT NULL,
  `name` varchar(100) NOT NULL,
  `gender` varchar(32) NOT NULL,
  `birthday` varchar(32) NOT NULL,
  `user_password` varchar(100) NOT NULL,
  `url_foto` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(32) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userid`, `user_name`, `user_level`, `name`, `gender`, `birthday`, `user_password`, `url_foto`, `status`, `create_by`, `create_at`) VALUES
(999999999, 'salman', 'admin', 'Salman, S.E., M.Si', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'admin', '0000-00-00 00:00:00'),
(1234567891, 'admin', 'admin', 'Admin The Mujaddid', 'Akhi', '1979-03-05', 'c4ca4238a0b923820dcc509a6f75849b', '86506+whatsapp-logo.png', 1, 'admin', '2019-06-06 19:14:33'),
(2013100483, 'dinda', 'pengurus', 'Cut Dinda Annisa', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2013100566, 'fajar', 'pengurus', 'M. Fajar Sidik', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '51208+M Fajar Sidik.jpg', 0, 'System', '2019-06-03 00:00:00'),
(2014100900, 'salvian', 'pengurus', 'Salvian Iman Kumara', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2014101000, 'nnsalida', 'pengurus', 'Annisa Nur Alida', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2014101007, 'caca', 'pengurus', 'Anissa Dyah Kartikasari', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2014101095, 'naufal', 'pengurus', 'Naufal Syarifuddin', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2014101196, 'atika', 'pengurus', 'Atika Leshwari', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2014101357, 'nurul', 'pengurus', 'Nurul Amalia Putri', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2014101365, 'beni', 'pengurus', 'Beni Rismanto', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2015101518, 'galuh', 'pengurus', 'Galuh Restu L', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2015101642, 'ekoeko', 'pengurus', 'Rizky Eko Harry S', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2015101753, 'istiqomah', 'pengurus', 'Istiqomah', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2015101786, 'dhani', 'pengurus', 'Dhani Anggoro M.N', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2015101794, 'kiekie', 'pengurus', 'Risky Suprayitno', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2015101820, 'mita', 'pengurus', 'Mita Dwi A.', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2015101894, 'arindra', 'pengurus', 'Arindra A.', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2015101955, 'daus', 'pengurus', 'Ahmad Oktavian Firdaus', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2015102044, 'fiqri', 'pengurus', 'Fiqri Hendrawan', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2015201767, 'ajeng', 'pengurus', 'Ajeng Setya R.', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2016102510, 'syamaidzar', 'anggota', 'Syamaidzar Agusetyawan', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2016102549, 'andri', 'pengurus', 'Andriana Pratama', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2016102599, 'erikaerika', 'pengurus', 'Erika Dona Pratiwi', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2016102606, 'mitaandriani', 'pengurus', 'Mita Andriani', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2016102659, 'ilhamazka', 'pengurus', 'Ilham Azka F', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2016102663, 'azizah', 'pengurus', 'Azizah Kusumadewi', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2016102814, 'puty', 'pengurus', 'Puty Nurusysyifa', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2016102928, 'alda', 'pengurus', 'Alda Naidiya', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2016102946, 'rahmilia', 'pengurus', 'Rahmilia Noviyanti', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2017103157, 'nuriman', 'anggota', 'Fadhilah Nur Iman', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2017103315, 'nandira', 'anggota', 'Nandira Rizky Nada S', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2017103499, 'addi', 'anggota', 'Addi Ammar K', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2017103523, 'rafa', 'pengurus', 'Rafa Nissa Azhari', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2017103537, 'bagus', 'pengurus', 'M Bagus Kurnia', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2017103583, 'hafizh', 'pengurus', 'M Hafizh Ramadhan', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2017103605, 'sahli', 'pengurus', 'M Maulidiy Sahli', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2017103733, 'andhika', 'pengurus', 'Andhika Abi Alfarisyi', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2017103746, 'kevin', 'pengurus', 'Kevin Nur H', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2017103886, 'arisaryanto', 'anggota', 'Aris Aryanto', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018103826, 'gilang', 'anggota', 'Gilang Fajri', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018103923, 'iiyar', 'anggota', 'Ridwan Ilyar Pratama', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018103943, 'adillah', 'anggota', 'Sabrina Adillah', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018103996, 'nuragita', 'anggota', 'Nur Agita', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018104059, 'andhikadwi', 'anggota', 'Andhika Dwi Prakoso', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018104179, 'anugraheka', 'anggota', 'Anugrah Eka Putra', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018104186, 'asyaastie', 'anggota', 'Asya Astie Anggi A', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018104207, 'istiazah', 'anggota', 'Istiazah P Putri', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018104218, 'herdaputri', 'anggota', 'Herda Putri Pangestu', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018104234, 'qurrota', 'anggota', 'Utha Qurrota A', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018104271, 'dhinda', 'anggota', 'Dhinda Firizky F', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018104307, 'lisnasiregar', 'anggota', 'Lisna Siregar', 'F', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018104352, 'muhammadnur', 'anggota', 'Muhammad Nur', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018104360, 'ariefc', 'anggota', 'Muhammad Arief C', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2018104379, 'robit', 'anggota', 'Robit Hussalam', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00'),
(2147483647, 'Salman', 'pengurus', 'Salman, S.E., M.Si', 'M', '', 'c4ca4238a0b923820dcc509a6f75849b', '', 0, 'System', '2019-06-03 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users_alamat`
--

CREATE TABLE `users_alamat` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(32) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_alamat`
--

INSERT INTO `users_alamat` (`id`, `userid`, `title`, `value`, `status`, `create_by`, `create_at`) VALUES
(9910, 1234567891, 'Kantor', 'dawdawdawdawd', 0, 'admin', '2019-06-04 01:48:07'),
(9911, 1234567891, 'Rumah', 'Jl. Pemuda Asli 1 RT01/03 Rawamangun, Pulogadung, Jakarta Timur, DKI Jakarta, 31220', 1, 'admin', '2019-06-03 02:00:00'),
(9912, 2013100566, 'Rumah', 'Jl. Pemuda Asli 1 RT01/03 Rawamangun, Pulogadung, Jakarta Timur, DKI Jakarta, 31220', 1, 'fajar', '2019-06-04 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users_kontak`
--

CREATE TABLE `users_kontak` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `title` varchar(32) NOT NULL,
  `value` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `create_by` varchar(32) NOT NULL,
  `create_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_kontak`
--

INSERT INTO `users_kontak` (`id`, `userid`, `title`, `value`, `status`, `create_by`, `create_at`) VALUES
(1110, 2013100566, 'Email', 'fajarkalbis@outlook.com', 1, 'fajar', '0000-00-00 00:00:00'),
(1111, 2013100566, 'No Hp', '6289288712212', 1, 'fajar', '2019-06-03 00:00:00'),
(1113, 1234567891, 'No Hp', '6289622874426', 0, 'admin', '2019-06-04 23:58:32'),
(9931, 1234567891, 'Email', 'halo@mujaddid.com', 1, 'admin', '2019-06-04 01:26:08');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_acara`
--
ALTER TABLE `tbl_acara`
  ADD PRIMARY KEY (`id_acara`);

--
-- Indexes for table `tbl_acara_dokumentasi`
--
ALTER TABLE `tbl_acara_dokumentasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_acara_kontak`
--
ALTER TABLE `tbl_acara_kontak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_dokumen`
--
ALTER TABLE `tbl_dokumen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_jurusan`
--
ALTER TABLE `tbl_jurusan`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `tbl_pendaftaran_acara`
--
ALTER TABLE `tbl_pendaftaran_acara`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `tbl_pendaftaran_anggota`
--
ALTER TABLE `tbl_pendaftaran_anggota`
  ADD PRIMARY KEY (`kode`);

--
-- Indexes for table `tbl_pendaftaran_bayar`
--
ALTER TABLE `tbl_pendaftaran_bayar`
  ADD PRIMARY KEY (`id_pembayaran`);

--
-- Indexes for table `tbl_periode`
--
ALTER TABLE `tbl_periode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_struktur`
--
ALTER TABLE `tbl_struktur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`);

--
-- Indexes for table `users_alamat`
--
ALTER TABLE `users_alamat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_kontak`
--
ALTER TABLE `users_kontak`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_acara_dokumentasi`
--
ALTER TABLE `tbl_acara_dokumentasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89741;

--
-- AUTO_INCREMENT for table `tbl_jurusan`
--
ALTER TABLE `tbl_jurusan`
  MODIFY `kode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9498;

--
-- AUTO_INCREMENT for table `tbl_pendaftaran_acara`
--
ALTER TABLE `tbl_pendaftaran_acara`
  MODIFY `kode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1024;

--
-- AUTO_INCREMENT for table `tbl_pendaftaran_anggota`
--
ALTER TABLE `tbl_pendaftaran_anggota`
  MODIFY `kode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9913;

--
-- AUTO_INCREMENT for table `tbl_periode`
--
ALTER TABLE `tbl_periode`
  MODIFY `id` int(32) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_struktur`
--
ALTER TABLE `tbl_struktur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
