<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-12 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-warning">
                  <div class="box-header with-border">
                    <h3 class="box-title">Tambah Alamat Pribadi</h3>
                  </div>
                  <form role="form" method="POST" action="personal-me-alamat-add.php" enctype="multipart/form-data">
                    <div class="box-body">
                      <div class="form-group">
                      <label for="exampleInputEmail1">Title</label>
                        <select class="form-control" id="title" name="title">
                          <option value="Rumah">Rumah</option>
                          <option value="Kantor">Kantor</option>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Detail Alamat</label>
                        <input type="text" class="form-control" id="value" name="value" required>
                      </div>
                      

                    </div>
                    <div class="box-footer">
                      <a href="personal-me.php" class="btn btn-primary">Kembali</a> 
                      <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                    </div>
                    <?php
                    if(isset($_POST["submit"])) {
                      
                      $id             = rand(1001,9999);
                      $userid         = $_SESSION['userid'];
                      $title          = $_POST['title'];
                      $value          = $_POST['value'];
                      $status         = 1;
                      $create_by      = $_SESSION['user_name'];
                      $create_at      = (new DateTime('now'))->format('Y-m-d H:i:s');
                      $sql = "INSERT INTO users_alamat (id, userid,  title, value, status, create_by, create_at)
                              VALUES ('$id','$userid','$title','$value', '$status', '$create_by', '$create_at')
                              ";
                      if ($connect-> query($sql) === TRUE ) {
                        echo "
                        <script type='text/javascript'>
                            alert('Alamat ".$title." Berhasil ditambah');
                            window.location = 'personal-me.php';
                        </script>";
                        } else {
                        echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                        }
                        $connect->close();
                        }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
