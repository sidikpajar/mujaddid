<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Detail Acara
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Update Poster Acara</a></li>
        <li class="active">disini</li>
      </ol>
    </section>


    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Update Poster Acara</h3>
              <a href="acara.php" class="btn btn-primary pull-right">Kembali</a>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <form role="form" method="POST" action="add.poster.php" enctype="multipart/form-data">
              <?php
                //ambil id_acara yang ada pada Skripsi/login/admin/add.poster.php?id_acara=.... 
                $id_acara = $_GET['id_acara'];

                //lakukan pengambilan data acara dengan menggunakan id_acara yang telah di ambil 
                $sql="SELECT * FROM tbl_acara  
                        where id_acara='$id_acara' ";
                $query = mysqli_query($connect,$sql);
                
                //tampilkan data-data hasil query
                while($row = mysqli_fetch_array($query)) {
              ?>
      
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputFile">Nama Acara</label>
                  <input class="form-control" type="text" value="<?php echo $row['nama_acara']?>" readonly>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Poster Acara</label>
                  <input type="file" name="poster" id="poster" required>
                </div>
              </div>
              <div class="box-footer">
                <input type="hidden" name="id_acara" value="<?php echo $row['id_acara']?>" />
                  <input type="hidden" name="nama_acara" value="<?php echo $row['nama_acara']?>" />
                <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
              </div>
              
              <?php } ?>
              <?php
              //require_once 'db_connect.php';
            if(isset($_POST["submit"])){
                $check = getimagesize($_FILES["poster"]["tmp_name"]);
                if($check !== false){
                    $poster = $_FILES['poster']['tmp_name'];
                    $imgContent = addslashes(file_get_contents($poster));
                    $id_acara = $_POST['id_acara'];
                    $nama_acara = $_POST['nama_acara'];


                    $insert = $connect->query("UPDATE tbl_acara SET  poster='$imgContent' WHERE id_acara = '$id_acara'");

                    if($insert){
                        echo "<script type= 'text/javascript'>
                              alert('Upload Poster Acara berhasil');
                              window.location = 'acara.php ';
                              </script>
                        ";
                    }else{
                        echo "<script type= 'text/javascript'>alert('File upload failed, please try again');</script>";
                    }


                }else{
                    echo "Please select an image file to upload.";
                }
            }
            ?>


            </form>
          </div>
      </div>
    </section>
  </div>


<?php include("layout/footer.php"); ?>
