<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-12 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Ubah Kegiatan Acara</h3>
                    <br/>
                    <a class="btn btn-warning" href="acara.php" >Kembali</a>
                  </div>
                  <form role="form" method="POST" action="edit.php" enctype="multipart/form-data">
                    <?php
                      //ambil id_acara yang ada pada Mujaddid/login/admin/add-poster.php?id_acara=.... 
                      $id_acara = $_GET['id_acara'];
                      //lakukan pengambilan data acara dengan menggunakan id_acara yang telah di ambil 
                      $sql="SELECT * FROM tbl_acara  
                              where id_acara='$id_acara' ";
                      $query = mysqli_query($connect,$sql);
                      
                      //tampilkan data-data hasil query
                      while($row = mysqli_fetch_array($query)) {
                    ?>
            
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Acara</label>
                        <input type="text" class="form-control" id="nama_acara" name="nama_acara" value="<?php echo $row['nama_acara']?>" required>
                      </div>
                      
                       <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Mulai</label>
                        <input type="datetime-local" class="form-control" id="tanggal_mulai" name="tanggal_mulai" value="<?php echo $row['tanggal_mulai']?>" required>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Selesai</label>
                        <input type="datetime-local" class="form-control" id="tanggal_selesai" name="tanggal_selesai" value="<?php echo $row['tanggal_selesai']?>" required>
                      </div>

                       <div class="form-group">
                        <label for="exampleInputEmail1">Tempat</label>
                        <input type="text" class="form-control" id="tempat" name="tempat" value="<?php echo $row['tempat']?>" required>
                        <input type="hidden" class="form-control" id="id_acara" name="id_acara" value="<?php echo $row['id_acara']?>" required>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Status</label>
                        <select class="form-control" id="status" name="status">
                          <option value="1">Aktif</option>
                          <option value="0">Non-Aktif</option>
                        </select>
                      </div>

                       <div class="form-group">
                        <label for="exampleInputEmail1">Deskripsi Acara</label>
                        <div class="box-body pad">
                          <textarea id="editor1" name="deskripsi" rows="10" cols="80" style="visibility: hidden; display:none;"> 
                           <?php echo $row['deskripsi']?>
                          </textarea>
                       </div>
                      </div>

                    </div>

                      <?php } ?>
                    <div class="box-footer">
                      <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                    </div>
                    <?php
                    if(isset($_POST["submit"])) {
                      $id_acara        = $_POST['id_acara'];
                      $nama_acara     = $_POST['nama_acara'];
                      $tanggal_mulai  = $_POST['tanggal_mulai'];
                      $tanggal_selesai= $_POST['tanggal_selesai'];
                      $tempat         = $_POST['tempat'];
                      $status         = $_POST['status'];;
                      $deskripsi      = $_POST['deskripsi'];
                      $create_by      = $_SESSION['user_name'];
                      $create_at      = (new DateTime('now'))->format('Y-m-d H:i:s');

                      $sql = "UPDATE users SET
                      nama_acara='$nama_acara' 
                      tanggal_mulai='$tanggal_mulai' 
                      tanggal_selesai='$tanggal_selesai' 
                      tempat='$tempat' 
                      status='$status' 
                      create_by='$create_by'  
                      create_at='$create_at'  
                      
                      WHERE id_acara = '$id_acara' ";

                        if ($connect-> query($sql) === TRUE ) {
                        echo "
                        <script type= 'text/javascript'>
                            alert('nama ".$nama_acara." Berhasil diUbah');
                            window.location = 'acara.php ';
                        </script>";

                        } else {
                        echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                        }
                        $connect->close();
                        }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>

   