<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-12 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Tambah Kegiatan Acara</h3>
                  </div>
                  <form role="form" method="POST" action="acara-add.php" enctype="multipart/form-data">
                    <div class="box-body">
                      <div class="form-group">
                      <label for="exampleInputEmail1">Periode</label>
                        <select class="form-control" id="id_periode" name="id_periode">
                          <?php
                            $show_periode = mysqli_query($connect, "SELECT * FROM tbl_periode where status = 1 ");
                            while($row = mysqli_fetch_array($show_periode)) {
                          ?>
                          <option value="<?php echo $row['id']; ?>" ><?php echo $row['periode']?></option>
                           <?php } ?>
                        </select>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Acara</label>
                        <input type="text" class="form-control" id="nama_acara" name="nama_acara" required>
                      </div>
                      
                       <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Mulai</label>
                        <input type="datetime-local" class="form-control" id="tanggal_mulai" name="tanggal_mulai" required>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Selesai</label>
                        <input type="datetime-local" class="form-control" id="tanggal_selesai" name="tanggal_selesai" required>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Pendaftaran</label>
                        <select class="form-control" id="pembayaran" name="pembayaran" onchange="yesnoCheck(this);">
                          <option value="0">Gratis</option>
                          <option value="1">Berbayar</option>
                        </select>
                      </div>


                      <script>
                        function yesnoCheck(that) {
                            if (that.value == "1") {
                                document.getElementById("ifYes").style.display = "block";
                            }
                             else {
                                document.getElementById("ifYes").style.display = "none";
                            }
                        }
                      </script>

                      <div class="form-group" id="ifYes" style="display: none;">
                        <div style="margin-left:50px">
                          <label for="exampleInputEmail1">Biaya Pendaftaran</label>
                          <input style="background-color:#BEFFB0;" type="number" class="form-control" id="biaya" name="biaya">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Tempat</label>
                        <input type="text" class="form-control" id="tempat" name="tempat" required>
                      </div>


                       <div class="form-group">
                        <label for="exampleInputEmail1">Deskripsi Acara</label>
                        <div class="box-body pad">
                          <textarea id="editor1" name="deskripsi" rows="10" cols="80" style="visibility: hidden; display:none;"> 
                          </textarea>
                       </div>
                      </div>

                    </div>
                    <div class="box-footer">
                      <a href="acara.php" class="btn btn-primary">Kembali</a> 
                      <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                    </div>
                    <?php
                    if(isset($_POST["submit"])) {
                      
                      $id_acara       = rand(1111,9999);
                      $id_periode     = $_POST['id_periode'];
                      $status         = 1;
                      $nama_acara     = $_POST['nama_acara'];
                      $tanggal_mulai  = $_POST['tanggal_mulai'];
                      $tanggal_selesai= $_POST['tanggal_selesai'];
                      $pembayaran     = $_POST['pembayaran'];
                      $biaya          = $_POST['biaya'];
                      $tempat         = $_POST['tempat'];
                      $deskripsi      = $_POST['deskripsi'];
                      $create_by      = $_SESSION['user_name'];
                      $create_at      = (new DateTime('now'))->format('Y-m-d H:i:s');
                      $sql = "INSERT INTO tbl_acara (id_acara, id_periode,  nama_acara, tanggal_mulai, tanggal_selesai, pembayaran, biaya, tempat, deskripsi, status, create_by, create_at)
                              VALUES ('$id_acara','$id_periode','$nama_acara','$tanggal_mulai', '$tanggal_selesai', '$pembayaran', '$biaya', '$tempat', '$deskripsi', '$status', '$create_by', '$create_at')
                              ";
                      if ($connect-> query($sql) === TRUE ) {
                        echo "
                        <script type='text/javascript'>
                            alert('Acara ".$nama_acara." Berhasil ditambah');
                            window.location = 'acara.php';
                        </script>";
                        } else {
                        echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                        }
                        $connect->close();
                        }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
