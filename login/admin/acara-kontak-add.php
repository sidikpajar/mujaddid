<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">

    <section class="content-header">
      <h1>
        Tambah PIC Kontak Acara
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Kontak Acara</a></li>
        <li class="active">disini</li>
      </ol>
    </section>


    <section class="content container-fluid">
      <div class="row">
        <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah PIC</h3>
             
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            
            <form role="form" method="POST" action="acara-kontak-add.php" enctype="multipart/form-data">
              
      
              <div class="box-body">
                
                <div class="form-group">
                  <label for="exampleInputFile">Nama Acara</label>
                    <?php
                      //ambil id_acara yang ada pada Mujaddid/login/admin/add-poster.php?id_acara=.... 
                      $id_acara = $_GET['id_acara'];
                      //lakukan pengambilan data acara dengan menggunakan id_acara yang telah di ambil 
                      $sql="SELECT * FROM tbl_acara  
                              where id_acara='$id_acara' ";
                      $query = mysqli_query($connect,$sql);
                      //tampilkan data-data hasil query
                      while($row = mysqli_fetch_array($query)) {
                    ?>
                      <input class="form-control" type="text" value="<?php echo $row['nama_acara']?>" readonly>
                      <input type="hidden" class="form-control" id="id_acara" name="id_acara" value="<?php echo $row['id_acara']?>" required>
                    <?php } ?>
                </div>

                <div class="form-group">
                      <label for="exampleInputEmail1">Pilih Anggota</label>
                        <select class="form-control select2" id="userid" name="userid">
                          <?php
                            $show_periode = mysqli_query($connect, "SELECT * FROM users Order by name asc ");
                            while($row = mysqli_fetch_array($show_periode)) {
                          ?>
                          <option value="<?php echo $row['userid']; ?>" ><?php echo $row['name']?></option>
                           <?php } ?>
                        </select>
                      </div>
                </div>

                

                <div class="box-footer">
                  <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                </div>
              
                <?php
                    if(isset($_POST["submit"])) {
                      
                      $id             = rand(1001,9999);
                      $id_acara       = $_POST['id_acara'];
                      $userid         = $_POST['userid'];
                      $status         = 1;
                      $create_by      = $_SESSION['user_name'];
                      $create_at      = (new DateTime('now'))->format('Y-m-d H:i:s');
                      $sql = "INSERT INTO tbl_acara_kontak (id, id_acara, userid, status, create_by, create_at)
                              VALUES ('$id','$id_acara','$userid', '$status', '$create_by', '$create_at')
                              ";
                      if ($connect-> query($sql) === TRUE ) {
                        echo "
                        <script type='text/javascript'>
                            alert('PIC Kontak Berhasil ditambah');
                            window.location = 'acara-detail.php?id_acara=".$id_acara."';
                        </script>";
                        } else {
                        echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                        }
                        $connect->close();
                        }
                    ?>


            </form>
          </div>
      </div>
    </section>
  </div>


<?php include("layout/footer.php"); ?>
