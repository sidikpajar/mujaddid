<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Daftar Acara
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Acara</li>
      </ol>
    </section>
    <section class="content-header">
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
             
              <div style="padding-top:20px">
                <a class="btn btn-primary" href="acara-add.php" >Tambah Acara</a>
              </div>
            </div>
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-12 table-responsive">
                      <iframe id="txtArea1" style="display:none"></iframe>
                      <table style=""  id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                          <th>Acara ID</th>
                          <th>Acara</th>
                          <th>Periode</th>
                          <th>Status</th>
                          <th>Pembayaran</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                            $sql="SELECT * FROM tbl_acara";
                            $query = mysqli_query($connect,$sql);
                            while($row = mysqli_fetch_array($query)) {
                            ?>
                          <tr role="row" class="odd">
                            <td ><?php echo $row['id_acara'] ?></td>
                            
                            <td><?php echo $row['nama_acara'] ?></td>
                            <td><?php $id_periode = $row['id_periode']; 
                                      $sql2="SELECT * FROM tbl_periode WHERE id='$id_periode' ";
                                      $query2 = mysqli_query( $connect, $sql2 );
                                        while($data = mysqli_fetch_array( $query2 )) {
                                          echo $data['periode'];
                                        } 
                                ?>
                            </td>
                            <td>
                            <?php 
                              // cek status, jika status 1 maka akan muncul Aktif, jika 0 maka muncul Tidak aktif
                              if ($row['status']==1){
                                echo "<a href='acara-update-to-off.php?id_acara=".$row['id_acara']." ' class='btn btn-success  btn-xs' '> Aktif </a>";
                              } else echo "<a href='acara-update-to-on.php?id_acara=".$row['id_acara']." ' class='btn btn-danger  btn-xs' '> Tidak Aktif </a>";
                              ?>
                            </td>
                            <td>
                              <?php 
                              // cek status, jika status 1 maka akan muncul Aktif, jika 0 maka muncul Tidak aktif
                              if ($row['pembayaran']==1){
                                echo "<a  class='btn btn-success  btn-xs' '> Berbayar - Rp".number_format($row['biaya'])." </a>";
                              } else echo "<a class='btn btn-default  btn-xs' '> Gratis </a>";
                              ?>
                            </td>
                            <td>
                              <?php
                                  echo "<a href='acara-detail.php?id_acara=".$row['id_acara']."' class='btn btn-xs btn-primary'>Detail</a>";
                              ?>
                              <?php
                                  echo "<a href='acara-edit.php?id_acara=".$row['id_acara']."' ' class='btn btn-xs   btn-warning'>Ubah Data</a>";
                              ?>
                            </td>
                          </tr>
                          <?php
                            }
                           ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  </div>
  <script>
      function printData()
        {
          var divToPrint=document.getElementById("example1");
          newWin= window.open("");
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
        }
  </script>
  <?php
  include("component/footer.php");
   ?>
