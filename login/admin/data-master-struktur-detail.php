<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Detail Struktur
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Acara</li>
      </ol>
    </section>
    <section class="content-header">
    </section>
    <section class="content">


    <!-- Level 1 -->
    <div class="row">


        <?php 
          $id = $_GET['id'];
          $Stuktur1="SELECT * FROM tbl_struktur 
          INNER JOIN users
          ON users.userid = tbl_struktur.nim
          INNER JOIN tbl_jurusan
          ON tbl_jurusan.kode = tbl_struktur.id_jurusan
          WHERE id_periode='$id' ";
          $queryStruktur1 = mysqli_query( $connect, $Stuktur1 );
          while($row = mysqli_fetch_array( $queryStruktur1 )) {
            if($row['lv1'] !== 0 && $row['lv2'] == 0 ){ 
        ?>
          <div class="col-md-3">
            <div class="box box-success">
              <ul class="nav nav-pills nav-stacked">
                <li><a href='#'>NIM <span class='pull-right'> <?php echo $row['nim'];?></span></a></li>
                <li><a href='#'>Nama Lengkap <span class='pull-right'> <?php echo $row['name'];?></span></a></li>
                <li><a href='#'>Jabatan <span class='pull-right'> <?php echo $row['jabatan'];?></span></a></li>
                <li><a href='#'>Program Studi <span class='pull-right'> <?php echo $row['nama_jurusan'];?></span></a></li>
              </ul>
            </div>
          </div>
        <?php 
            }
          } 
        ?>
    </div>


  <!-- Level 2 -->
    <div class="row">
      
        <?php 
          $Stuktur2="SELECT * FROM tbl_struktur 
          INNER JOIN users
          ON users.userid = tbl_struktur.nim
          INNER JOIN tbl_jurusan
          ON tbl_jurusan.kode = tbl_struktur.id_jurusan
          WHERE id_periode='$id' AND lv2 != 0 ";

          $queryStruktur2 = mysqli_query( $connect, $Stuktur2 );
          while($row = mysqli_fetch_array( $queryStruktur2 )) {
            if($row['lv1'] !== 0 && $row['lv2'] !== 0 && $row['lv3'] == 0 ){
        ?>
          <div class="col-md-3">
            <div class="box box-primary">
              <ul class="nav nav-pills nav-stacked">
                <li><a href='#'>NIM <span class='pull-right'> <?php echo $row['nim'];?></span></a></li>
                <li><a href='#'>Nama Lengkap <span class='pull-right'> <?php echo $row['name'];?></span></a></li>
                <li><a href='#'>Jabatan <span class='pull-right'> <?php echo $row['jabatan'];?></span></a></li>
                <li><a href='#'>Program Studi <span class='pull-right'> <?php echo $row['nama_jurusan'];?></span></a></li>
              </ul>
            </div>
          </div>
        <?php 
            }
          } 
        ?>
    </div>

    
    <!-- Level 3 -->
    <div class="row">
      
        <?php 
          
          $Stuktur3="SELECT * FROM tbl_struktur 
          INNER JOIN users
          ON users.userid = tbl_struktur.nim
          INNER JOIN tbl_jurusan
          ON tbl_jurusan.kode = tbl_struktur.id_jurusan
          WHERE id_periode='$id' AND lv3 != 0 ";

          $queryStruktur3 = mysqli_query( $connect, $Stuktur3 );
          while($row = mysqli_fetch_array( $queryStruktur3 )) {
            if($row['lv1'] !== 0 && $row['lv2'] !== 0 && $row['lv3'] !== 0 && $row['lv4'] == 0 ){
        ?>
          <div class="col-md-3">
            <div class="box box-warning">
              <ul class="nav nav-pills nav-stacked">
                <li><a href='#'>NIM <span class='pull-right'> <?php echo $row['nim'];?></span></a></li>
                <li><a href='#'>Nama Lengkap <span class='pull-right'> <?php echo $row['name'];?></span></a></li>
                <li><a href='#'>Jabatan <span class='pull-right'> <?php echo $row['jabatan'];?></span></a></li>
                <li><a href='#'>Program Studi <span class='pull-right'> <?php echo $row['nama_jurusan'];?></span></a></li>
              </ul>
            </div>
          </div>
        <?php 
            }
          } 
        ?>
      
    </div>


    <!-- Level 4 -->
    <div class="row">
        <?php 
          $Stuktur4="SELECT * FROM tbl_struktur 
          INNER JOIN users
          ON users.userid = tbl_struktur.nim
          INNER JOIN tbl_jurusan
          ON tbl_jurusan.kode = tbl_struktur.id_jurusan
          WHERE id_periode='$id' AND lv4 != 0 ";

          $queryStruktur4 = mysqli_query( $connect, $Stuktur4 );
          while($row = mysqli_fetch_array( $queryStruktur4 )) {
            if($row['lv1'] !== 0 && $row['lv2'] !== 0 && $row['lv3'] !== 0 && $row['lv4'] !== 0 && $row['lv5'] == 0 ){
        ?>
          <div class="col-md-3">
            <div class="box box-danger">
              <ul class="nav nav-pills nav-stacked">
                <li><a href='#'>NIM <span class='pull-right'> <?php echo $row['nim'];?></span></a></li>
                <li><a href='#'>Nama Lengkap <span class='pull-right'> <?php echo $row['name'];?></span></a></li>
                <li><a href='#'>Jabatan <span class='pull-right'> <?php echo $row['jabatan'];?></span></a></li>
                <li><a href='#'>Program Studi <span class='pull-right'> <?php echo $row['nama_jurusan'];?></span></a></li>
              </ul>
            </div>
          </div>
        <?php 
            }
          } 
        ?>
    </div>

    </section>
  </div>
  </div>
  <script>
      function printData()
        {
          var divToPrint=document.getElementById("example1");
          newWin= window.open("");
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
        }
  </script>
  <?php
  include("component/footer.php");
   ?>
