<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Pendaftaran Calon Anggota The Mujaddid
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Pendaftaran</li>
      </ol>
    </section>
    <section class="content-header">
    </section>
    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-primary">
            <div class="box-header">
            </div>
            <div class="box-body">
              <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                  <div class="row">
                    <div class="col-sm-12 table-responsive">
                      <iframe id="txtArea1" style="display:none"></iframe>
                      <table style=""  id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                        <thead>
                        <tr role="row">
                          <th>Kode Pendaftaran</th>
                          <th>NIM</th>
                          <th>Nama</th>
                          <th>Jurusan</th>
                          <th>Alasan</th>
                          <th>Email</th>
                          <th>Tanggal Daftar</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                          <?php
                            $sql="SELECT * FROM tbl_pendaftaran_anggota";
                            $query = mysqli_query($connect,$sql);
                            while($row = mysqli_fetch_array($query)) {
                            ?>
                          <tr role="row" class="odd">
                            <td ><?php echo $row['kode'] ?></td>
                            <td ><?php echo $row['nim'] ?></td>
                            <td ><?php echo $row['nama'] ?></td>
                            <td ><?php echo $row['jurusan'] ?></td>
                            <td ><?php echo $row['alasan'] ?></td>
                            <td ><?php echo $row['email'] ?></td>
                            <td ><?php echo $row['tanggal_daftar'] ?></td>
                            <td>
                              <?php
                                  echo "<a href='pendaftaran-terima.php?kode=".$row['kode']."' class='btn btn-xs btn-primary'>Terima</a>";
                              ?>
                              <?php
                                  echo "<a href='pendaftaran-tolak.php?kode=".$row['kode']."' ' class='btn btn-xs   btn-danger'>Tolak</a>";
                              ?>
                            </td>
                          </tr>
                          <?php
                            }
                           ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div> 
              <div class="box-footer">
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>
  </div>
  <script>
      function printData()
        {
          var divToPrint=document.getElementById("example1");
          newWin= window.open("");
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
        }
  </script>
  <?php
  include("component/footer.php");
   ?>
