<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-12 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Ubah Kegiatan Acara</h3>
                    <br/>
                   
                  </div>
                    <?php
                      $id_acara = $_GET['id_acara'];
                      $show_user = mysqli_query($connect,"SELECT * FROM tbl_acara WHERE id_acara='$id_acara' ");
                      while($row = mysqli_fetch_array($show_user)) {
                    ?>
                  <form role="form" method="POST" action="acara-edit.php" enctype="multipart/form-data">
                    <div class="box-body">
                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Acara</label>
                        <input type="text" class="form-control" id="nama_acara" name="nama_acara" value="<?php echo $row['nama_acara']?>" required>
                        <input type="hidden" class="form-control" id="id_acara" name="id_acara" value="<?php echo $row['id_acara']?>" required>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Mulai <i style="color:green"><small>- Format (dd/mm/yyyy h:i:s)</small></i></label>
                        <input type="text" class="form-control" id="tanggal_mulai" name="tanggal_mulai" value="<?php echo $row['tanggal_mulai']?>" required>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Tanggal Selesai <i style="color:green"><small>- Format (dd/mm/yyyy h:i:s)</small></i></label>
                        <input type="text" class="form-control" id="tanggal_selesai" name="tanggal_selesai" value="<?php echo $row['tanggal_selesai']?>" required>
                      </div>

                       <div class="form-group">
                        <label for="exampleInputEmail1">Tempat</label>
                        <input type="text" class="form-control" id="tempat" name="tempat" value="<?php echo $row['tempat']?>" required>
                        <input type="hidden" class="form-control" id="id_acara" name="id_acara" value="<?php echo $row['id_acara']?>" required>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Pendaftaran</label>
                        <select class="form-control" id="pembayaran" name="pembayaran" onchange="yesnoCheck(this);">
                          <option value="0">Gratis</option>
                          <option value="1">Berbayar</option>
                        </select>
                      </div>


                      <script>
                        function yesnoCheck(that) {
                            if (that.value == "1") {
                                document.getElementById("ifYes").style.display = "block";
                            }
                             else {
                                document.getElementById("ifYes").style.display = "none";
                            }
                        }
                      </script>

                      <div class="form-group" id="ifYes" style="display: none;">
                        <div style="margin-left:50px">
                          <label for="exampleInputEmail1">Biaya Pendaftaran</label>
                          <input style="background-color:#BEFFB0;" type="number" class="form-control" id="biaya" name="biaya" value="<?php echo $row['biaya']?>">
                        </div>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Status</label>
                        <select class="form-control" id="status" name="status">
                          <option value="1">Aktif</option>
                          <option value="0">Non-Aktif</option>
                        </select>
                      </div>

                       <div class="form-group">
                        <label for="exampleInputEmail1">Deskripsi Acara</label>
                        <div class="box-body pad">
                          <textarea id="editor1" name="deskripsi" rows="10" cols="80" style="visibility: hidden; display:none;"> 
                           <?php echo $row['deskripsi']?>
                          </textarea>
                       </div>
                      </div>

                    </div>
                    <div class="box-footer">
                      <a class="btn btn-warning" href="acara.php" >Kembali</a>
                      <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                    </div>
                  </form>
                  <?php } ?>
                  <?php
                  if(isset($_POST["submit"])) {
                    $id_acara           = $_POST['id_acara'];
                    $nama_acara         = $_POST['nama_acara'];
                    $tanggal_mulai      = $_POST['tanggal_mulai'];
                    $tanggal_selesai    = $_POST['tanggal_selesai'];
                    $tempat             = $_POST['tempat'];
                    $pembayaran         = $_POST['pembayaran'];
                    $biaya         = $_POST['biaya'];
                    $status             = $_POST['status'];
                    $deskripsi          = $_POST['deskripsi'];
                    $create_by          = $_SESSION['user_name'];
                    $create_at          = (new DateTime('now'))->format('Y-m-d H:i:s');

                    $sql = "UPDATE tbl_acara SET
                    nama_acara='$nama_acara',
                    tanggal_mulai='$tanggal_mulai', 
                    tanggal_selesai='$tanggal_selesai', 
                    pembayaran='$pembayaran',
                    biaya='$biaya',
                    tempat='$tempat',
                    deskripsi='$deskripsi', 
                    status='$status',
                    create_by='$create_by',
                    create_at='$create_at'
                    WHERE id_acara = '$id_acara' ";

                      if ($connect-> query($sql) === TRUE ) {
                      echo "
                      <script type= 'text/javascript'>
                          alert('Acara ".$nama_acara." Berhasil diubah');
                          window.location = 'acara.php ';
                      </script>";

                      } else {
                      echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                      }
                      $connect->close();
                      }

                  
                  ?>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
