<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-12 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Tambah Jurusan</h3>
                  </div>
                  <form role="form" method="POST" action="data-master-jurusan-add.php" enctype="multipart/form-data">
                    <div class="box-body">
                      

                      <div class="form-group">
                        <label for="exampleInputEmail1">Nama Jurusan</label>
                        <input type="text" class="form-control" id="nama_jurusan" name="nama_jurusan" required>
                      </div>
                
                    </div>
                    <div class="box-footer">
                      <a href="acara.php" class="btn btn-primary">Kembali</a> 
                      <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                    </div>
                    <?php
                    if(isset($_POST["submit"])) {
                      
                      $kode           = rand(1111,9999);
                      $nama_jurusan   = $_POST['nama_jurusan'];
                      $status         = 1;
                      $create_by      = $_SESSION['user_name'];
                      $create_at      = (new DateTime('now'))->format('Y-m-d H:i:s');
                      $sql = "INSERT INTO tbl_jurusan (kode, nama_jurusan,  status, create_by, create_at)
                              VALUES ('$kode','$nama_jurusan','$status', '$create_by', '$create_at')
                              ";
                      if ($connect-> query($sql) === TRUE ) {
                        echo "
                        <script type='text/javascript'>
                            alert('Jurusan ".$nama_jurusan." Berhasil ditambah');
                            window.location = 'data-master-jurusan.php';
                        </script>";
                        } else {
                        echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                        }
                        $connect->close();
                        }
                    ?>
                  </form>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
