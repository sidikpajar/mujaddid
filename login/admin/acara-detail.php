<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
<div class="content-wrapper">
	<section class="content-header">
	<h1>Detail Acara The Mujaddid <a href="acara.php" class="btn btn-primary">Kembali</a></h1> 
	<ol class="breadcrumb">
		<li>
			<a href="index.php"><i class="fa fa-dashboard"></i> Home</a>
		</li>
		<li class="active">Detail Acara</li>
	</ol>
	</section>
	<section class="content-header"></section>
	<section class="content">
	<div class="row" style="margin:10px">
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active">
          <a href="#acara" data-toggle="tab" aria-expanded="true">Acara</a>
        </li>
        <li class="">
          <a href="#deskripsi" data-toggle="tab" aria-expanded="true">Deskripsi Acara</a>
        </li>
        <li class="">
          <a href="#kontak" data-toggle="tab" aria-expanded="true">Kontak</a>
        </li>
        <li class="">
          <a href="#poster" data-toggle="tab" aria-expanded="true">Poster</a>
        </li>
        <li class="">
          <a href="#pendaftar" data-toggle="tab" aria-expanded="false">Pendaftar</a>
        </li>
        <li class="">
          <a href="#dokumen" data-toggle="tab" aria-expanded="false">Dokumen</a>
        </li>
        <li class="">
          <a href="#foto" data-toggle="tab" aria-expanded="false">Dokumentasi Foto</a>
        </li>
        
      </ul>
      <div class="tab-content">

        <div class="tab-pane active" id="acara">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th>Keterangan</th>
                <th>Isi</th>
              </tr>
                <?php
                  $id_acara = $_GET['id_acara'];
                  $sql1="SELECT * FROM tbl_acara WHERE id_acara='$id_acara' ";
                  $query = mysqli_query( $connect, $sql1 );
                  while($row = mysqli_fetch_array( $query )) {
                ?>
                  <tr>
                    <td>ID Acara</td>
                    <td>
                      <?php echo $row['id_acara']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Periode</td>
                    <td>
                      <?php
                        $id_periode = $row['id_periode']; 
                        $sql2="SELECT * FROM tbl_periode WHERE id='$id_periode' ";
                        $query2 = mysqli_query( $connect, $sql2 );
                          while($data = mysqli_fetch_array( $query2 )) {
                            echo $data['periode'];
                          }
                        ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Nama Acara</td>
                    <td>
                      <?php echo $row['nama_acara']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Tanggal Mulai</td>
                    <td>
                      <?php echo $row['tanggal_mulai']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Tanggal Selesai</td>
                    <td>
                      <?php echo $row['tanggal_selesai']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Tempat</td>
                    <td>
                      <?php echo $row['tempat']; ?>
                    </td>
                  </tr>
                  <tr>
                    <td>Status</td>
                    <td>
                      <?php 
                        // cek status, jika status 1 maka akan muncul Aktif, jika 0 maka muncul Tidak aktif
                        if ($row['status']==1){
                          echo "<a class='btn btn-success btn-xs' '>Aktif</a>"; } else echo "<a class='btn btn-danger btn-xs' '>Tidak Aktif</a>"; 
                      ?>
                    </td>
                  </tr>
                <?php } ?>
              </tbody>
            </table>
        </div>

        <div class="tab-pane" id="deskripsi">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th>deskripsi</th>
              </tr>
              <?php
                  $id_acara = $_GET['id_acara'];
                  $sql2="SELECT * FROM tbl_acara
                  WHERE id_acara='$id_acara' ";
                  $query2 = mysqli_query( $connect, $sql2 );
                  while($row = mysqli_fetch_array( $query2 )) {
                ?>
              <tr>
                <td>
                  <?php echo $row['deskripsi']; ?>
                </td>
              </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>

        <div class="tab-pane" id="kontak">
          <?php
            $id_acara = $_GET['id_acara'];
            echo "<a style='margin:10px' class='btn btn-primary btn-xs' href='acara-kontak-add.php?id_acara=".$id_acara." '> Tambah PIC Kontak </a>";
          ?>
          <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
              <div class="row">
                <div class="col-md-12 table-responsive">
                  <?php
                    $id_acara = $_GET['id_acara'];
                    $sql_kontak="SELECT url_foto, tbl_acara_kontak.id AS id_kontak, users.userid AS NIM, users.name AS NAMA FROM tbl_acara_kontak
                    INNER JOIN users
                    ON users.userid = tbl_acara_kontak.userid
                    WHERE tbl_acara_kontak.id_acara = '$id_acara' ";
                    
                    $query_kontak = mysqli_query( $connect, $sql_kontak );
                    while($row = mysqli_fetch_array( $query_kontak )) {
                  ?>

                      <div class="col-md-4">
                        <div class="box box-default">
                          <div class="box-header with-border">
                            <h3 class="box-title"><?php echo $row['NAMA']; ?></h3>
                            <div class="box-tools pull-right">
                              <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                              </button>
                              <a href="acara-kontak-delete.php?id=<?php echo $row['id_kontak']; ?>&id_acara=<?php echo $id_acara ?>" class="btn btn-box-tool"><i class="fa fa-times"></i></a>
                            </div>
                          </div>
                          <div class="box-body">
                            <div class="row">
                              <div class="col-md-12" style="text-align:center">
                                <img src="../files-photo/<?php echo $row['url_foto'];?>">
                                
                              </div>
                            </div>
                          </div>
                          <div class="box-footer no-padding">
                            <ul class="nav nav-pills nav-stacked">
                              <?php
                                $nim = $row['NIM'];
                                $sql_detail_kontak = "Select * from users_kontak WHERE userid ='$nim' ";
                                $query_detail_kontak = mysqli_query( $connect, $sql_detail_kontak );
                                while($data_kontak = mysqli_fetch_array( $query_detail_kontak )) {
                                  echo "<li><a href='#'>".$data_kontak['title']." <span class='pull-right text-green'> ".$data_kontak['value']."</span></a></li>";
                                }
                              ?>
                              
                            </ul>
                          </div>
                        </div>
                      </div>

                  <?php } ?>

                  
                  
                </div>
              </div>
            </div>
        </div>



        <div class="tab-pane" id="poster">
          <?php
            $id_acara = $_GET['id_acara'];
            $show_alamat = mysqli_query($connect,"SELECT poster FROM tbl_acara WHERE id_acara='$id_acara' ");
            while($row = mysqli_fetch_array($show_alamat)) {
              if($row['poster']==''){
                echo "<a style='margin:10px' class='btn btn-primary btn-xs' href='acara-poster-add.php?id_acara=".$id_acara." '> Tambah Poster </a>";
              } else {
                echo "<a style='margin:10px' class='btn btn-warning btn-xs' href='acara-poster-add.php?id_acara=".$id_acara." '> Ubah Poster </a>";
              }
            }
          ?>
          <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
              <div class="row">
                <div class="col-md-12 table-responsive">
                  <?php
                    $id_acara = $_GET['id_acara'];
                    $sql_poster="SELECT poster FROM tbl_acara
                    WHERE id_acara = '$id_acara' ";
                    $query_poster = mysqli_query( $connect, $sql_poster );
                    while($row = mysqli_fetch_array( $query_poster )) {
                  ?>
                      <div class="col-md-4">
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12" style="text-align:center">
                              <img class="img-responsive" src="../files-poster/<?php echo $row['poster'];?>">
                              
                            </div>
                          </div>
                        </div>
                      </div>
                  <?php } ?>
                </div>
              </div>
            </div>
        </div>




        <div class="tab-pane" id="pendaftar">
            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
              <div class="row">
                <div class="col-sm-12 table-responsive">
                  <iframe id="txtArea1" style="display:none"></iframe>
                  <table style=""  id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                    <thead>
                    <tr role="row">
                      <th>Kode Pendaftaran</th>
                      <th>Nama Acara</th>
                      <th>Nama Pendaftar</th>
                      <th>Email</th>
                      <th>Institute</th>
                      <th>Tanggal Daftar</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                        $id_acara = $_GET['id_acara'];
                        $sql3="SELECT 
                          tbl_pendaftaran_acara.kode as KodePendaftaran,
                          tbl_acara.nama_acara as NamaAcara,
                          tbl_pendaftaran_acara.nama as NamaPendaftar,
                          tbl_pendaftaran_acara.email as Email,
                          tbl_pendaftaran_acara.institute as Institute,
                          tbl_pendaftaran_acara.tanggal_daftar as TanggalDaftar
                        FROM tbl_pendaftaran_acara
                        INNER JOIN tbl_acara
                          ON tbl_acara.id_acara = tbl_pendaftaran_acara.id_acara
                        WHERE tbl_acara.id_acara='$id_acara' ";
                        $query3 = mysqli_query( $connect, $sql3 );
                        while($row = mysqli_fetch_array( $query3 )) {
                      ?>
                      <tr>
                        <td>
                          <?php echo $row['KodePendaftaran']; ?></td>
                        <td>
                          <?php echo $row['NamaAcara']; ?></td>
                        <td>
                          <?php echo $row['NamaPendaftar']; ?></td>
                        <td>
                          <?php echo $row['Email']; ?></td>
                        <td>
                          <?php echo $row['Institute']; ?></td>
                        <td>
                          <?php echo $row['TanggalDaftar']; ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        
        </div>

        <div class="tab-pane" id="dokumen">
          <a href="" class="btn btn-primary btn-xs" style="margin:10px">Tambah Dokumen</a>
          <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
              <div class="row">
                <div class="col-sm-12 table-responsive">
                  <iframe id="txtArea1" style="display:none"></iframe>
                  <table style=""  id="example0" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                    <thead>
                    <tr role="row">
                      <th>Kode Pendaftaran</th>
                      <th>Nama Acara</th>
                      <th>Nama Pendaftar</th>
                      <th>Email</th>
                      <th>Institute</th>
                      <th>Tanggal Daftar</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php
                        $id_acara = $_GET['id_acara'];
                        $sql3="SELECT 
                          tbl_pendaftaran_acara.kode as KodePendaftaran,
                          tbl_acara.nama_acara as NamaAcara,
                          tbl_pendaftaran_acara.nama as NamaPendaftar,
                          tbl_pendaftaran_acara.email as Email,
                          tbl_pendaftaran_acara.institute as Institute,
                          tbl_pendaftaran_acara.tanggal_daftar as TanggalDaftar
                        FROM tbl_pendaftaran_acara
                        INNER JOIN tbl_acara
                          ON tbl_acara.id_acara = tbl_pendaftaran_acara.id_acara
                        WHERE tbl_acara.id_acara='$id_acara' ";
                        $query3 = mysqli_query( $connect, $sql3 );
                        while($row = mysqli_fetch_array( $query3 )) {
                      ?>
                      <tr>
                        <td>
                          <?php echo $row['KodePendaftaran']; ?></td>
                        <td>
                          <?php echo $row['NamaAcara']; ?></td>
                        <td>
                          <?php echo $row['NamaPendaftar']; ?></td>
                        <td>
                          <?php echo $row['Email']; ?></td>
                        <td>
                          <?php echo $row['Institute']; ?></td>
                        <td>
                          <?php echo $row['TanggalDaftar']; ?></td>
                      </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
        </div>

        <div class="tab-pane" id="foto">
        <?php
            $id_acara = $_GET['id_acara'];
            echo "<a style='margin:10px' class='btn btn-primary btn-xs' href='acara-dokumentasi-add.php?id_acara=".$id_acara." '> Tambah Foto </a>";
          ?>
          <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
              <div class="row">
                <div class="col-md-12 table-responsive">
                  <?php
                    $id_acara = $_GET['id_acara'];
                    $sql_poster="SELECT * FROM tbl_acara_dokumentasi 
                    WHERE id_acara = '$id_acara' ";
                    $query_poster = mysqli_query( $connect, $sql_poster );
                    while($row = mysqli_fetch_array( $query_poster )) {
                  ?>
                      <div class="col-md-4">
                        <div class="box-body">
                          <div class="col-md-12" style="text-align:center">
                            <img class="img-responsive" src="../files-dokumentasi/<?php echo $row['url_foto'];?>"></br>
                            <i><?php echo $row['judul'] ?></i><a style="margin:5px" href="acara-dokumentasi-delete.php?id=<?php echo $row['id']; ?>&id_acara=<?php echo $id_acara ?>" class=""><i style="color:red" class="fa fa-times"></i></a>
                          </div>
                        </div>
                      </div>
                  <?php } ?>
                </div>
              </div>
            </div>
        </div>




      </div>
    </div>
	</div>
	</section>
</div>
</div>
<script>
      function printData()
        {
          var divToPrint=document.getElementById("example1");
          newWin= window.open("");
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
        }
  </script>
<?php
  include("component/footer.php");
   ?>