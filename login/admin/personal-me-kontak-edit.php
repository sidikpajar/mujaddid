<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
  <div class="content-wrapper">
    <section class="content container-fluid">
        <div class="row">
          <div class="col-md-12 shadow-lg">
            <div class="box box-widget widget-user">
                <div class="box box-success">
                  <div class="box-header with-border">
                    <h3 class="box-title">Ubah Kontak</h3>
                    <br/>
                   
                  </div>
                    <?php
                      $id = $_GET['id'];
                      $show_kontak = mysqli_query($connect,"SELECT * FROM users_kontak WHERE id='$id' ");
                      while($row = mysqli_fetch_array($show_kontak)) {
                    ?>
                  <form role="form" method="POST" action="personal-me-kontak-edit.php" enctype="multipart/form-data">
                    <div class="box-body">

                      <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <select class="form-control" id="title" name="title">
                          <option value="No Hp">Nomor Handphone</option>
                          <option value="Email">Email</option>
                          <option value="Instagram">Instagram</option>
                          <option value="Line">Line</option>
                        </select>
                      </div>
                      
                      <input type="hidden" class="form-control" id="id" name="id" value="<?php echo $row['id']?>" required>

                      
                       <div class="form-group">
                        <label for="exampleInputEmail1">Detail Kontak</label>
                        <input type="text" class="form-control" id="value" name="value" value="<?php echo $row['value']?>" required>
                      </div>

                      <div class="form-group">
                        <label for="exampleInputEmail1">Status</label>
                        <select class="form-control" id="status" name="status">
                          <option value="1">Aktifkan Alamat</option>
                          <option value="0">Non-Aktifkan Alamat</option>
                        </select>
                      </div>
          
                    </div>
                    <div class="box-footer">
                      <a class="btn btn-warning" href="personal-me.php" >Kembali</a>
                      <button type="submit" name="submit" class="btn btn-success pull-right">Submit</button>
                    </div>
                  </form>
                  <?php } ?>
                  <?php
                  if(isset($_POST["submit"])) {
                    $id                 = $_POST['id'];
                    $title              = $_POST['title'];
                    $value              = $_POST['value'];
                    $status             = $_POST['status'];
                    $create_by          = $_SESSION['user_name'];
                    $create_at          = (new DateTime('now'))->format('Y-m-d H:i:s');

                    $sql = "UPDATE users_kontak SET
                    title='$title',
                    value='$value', 
                    status='$status',
                    create_by='$create_by',
                    create_at='$create_at'
                    WHERE id = '$id' ";

                      if ($connect-> query($sql) === TRUE ) {
                      echo "
                      <script type= 'text/javascript'>
                          alert('Alamat ".$title." Berhasil diubah');
                          window.location = 'personal-me.php ';
                      </script>";

                      } else {
                      echo "<script type= 'text/javascript'>alert('Error: " . $sql . "<br>" . $connect->error."');</script>";
                      }
                      $connect->close();
                      }

                  
                  ?>
                </div>
            </div>

          </div>

        </div>
    </section>
  </div>
  <?php
  include("component/footer.php");
   ?>
