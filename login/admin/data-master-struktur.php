
<?php
  include("component/header.php");
  include("component/sidebar.php");
?>
<style>
.card {
margin-top:30px;
box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
transition: 0.3s;
border-radius: 5px;
background-color: white;
}
.box{
box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
transition: 0.3s;
border-radius: 5px;
background-color: white;
}
.box:hover{
box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);

}

.card h4{
padding:20px 10px;
text-align: center;
}

.card:hover {
box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

.card-form {
box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
transition: 0.3s;
border-radius: 5px;
background-color: white;
}
.card-form p{
padding:20px 10px;
text-align: center;

}

.card-form:hover {
box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2);
}

img {
border-radius: 5px 5px 0 0;
}

.container {
padding: 2px 16px;
}


</style>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Data Struktur The Mujaddid
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Data Master</a></li>
        <li class="active">Struktur Organisasi</li>
      </ol>
    </section>
    <section class="content-header">
    </section>
    <section class="content">
    <div class="row">

    <?php
      $sql="SELECT * FROM tbl_periode";
      $query = mysqli_query($connect,$sql);
      while($row = mysqli_fetch_array($query)) {
    ?>
        <a href="data-master-struktur-detail.php?id=<?php echo $row['id'] ?>">
          <div class="col-md-2 col-xs-6">
            <div class="card">
              <img src="../dist/img/sd/db.jpg" alt="Avatar" style="width:100%">
              <h4><b><?php echo $row['periode']?></b></h4>
            </div>
          </div>
        </a>

      <?php } ?>
        
        </div>
    </section>
  </div>
  </div>
  <script>
      function printData()
        {
          var divToPrint=document.getElementById("example1");
          newWin= window.open("");
          newWin.document.write(divToPrint.outerHTML);
          newWin.print();
          newWin.close();
        }
  </script>
  <?php
  include("component/footer.php");
   ?>
